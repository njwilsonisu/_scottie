﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;
using UnityEngine.SceneManagement;
using VRTK;

namespace SCOTTIE
{
    public class PhotonUser : MonoBehaviour
    {
        public enum userTypes
        {
            Undefined,
            Instructor,
            Apprentice
        }

        //public userTypes userType;

        public PhotonView PV;

        private NetworkManager networkManager;

        public NetworkManager NetworkManager
        {
            get
            {
                if (networkManager == null)
                {
                    // Try getting the network manager
                    networkManager = NetworkManager.instance;
                }

                return networkManager;
            }
        }

        // Start is called before the first frame update
        void Start()
        {
            Debug.Log("PhotonUser: Start");

            PV = GetComponent<PhotonView>();

            if (PV != null)
            {
                if (PV.IsMine)
                {
                    Debug.Log("PhotonUser: PhotonView is Mine");

                    DontDestroyOnLoad(gameObject);

                    PV.RPC("RPC_SetNickName", RpcTarget.AllBuffered, PhotonNetwork.NickName);

                }
            }
        }


        [PunRPC]
        void RPC_SetNickName(string nName)
        {
            Debug.Log("PhotonUser: RPC_SetNickName " + nName);
            gameObject.name = nName;

            if (NetworkManager)
            {
                transform.parent = NetworkManager.transform;

                Debug.Log("PhotonUser: Set NetworkManager as parent");
            }
        }
    }
}