﻿using System.Collections;
using System.Collections.Generic;
using Microsoft.MixedReality.Toolkit.Input;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using UnityEngine.EventSystems;
using VRTK;

namespace SCOTTIE
{
    // This Script manages how the Apprentice is represented in the scene

    public class AvatarSync : MonoBehaviour, IPunObservable
    {
        private PhotonView PV;

        // Items used for updating the avatars position
        [SerializeField]
        private Camera mainCamera;

        // higher value -> faster update
        public float SmoothingDelay = 10;

        public Transform AvatarHead;

        public GameObject AvatarBody;

        //public bool myFlash = false;
        //public bool flash = false;
        //public GameObject FlashObject;

        public bool trackPosition = true;

        private Vector3 networkAvatarLocalPosition;
        private Quaternion networkAvatarLocalRotation;
        private Quaternion networkAvatarHeadLocalRotation;

        void Start()
        {
            PV = GetComponent<PhotonView>();

            mainCamera = Camera.main;

            if (PV.IsMine)
            {
                Debug.Log("AvatarSync: PhotonView is Mine");

                AvatarHead.gameObject.SetActive(false);
                AvatarBody.SetActive(false);
            }

        }

        void IPunObservable.OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
        {
            
            if (stream.IsWriting)
            {
                // THIS IS OUR LOCAL SCENE --> we need to send the information

                // We want to send the local position and rotation of the avatar (who is following the position of the camera)

                if (trackPosition)
                {
                    // Stream the cameras position
                    stream.SendNext(transform.localPosition);
                }
                else
                {
                    //Debug.Log("AvatarSync: Not Sending Position");
                }

                //stream.SendNext(transform.localPosition);
                stream.SendNext(transform.localRotation);
                stream.SendNext(AvatarHead.localRotation);


                //stream.SendNext(myFlash);

            }
            else
            {
                // THIS IS NOT OUR LOCAL SCENE --> we need to receive the information

                if (trackPosition)
                {
                    // Receive the avatars local position
                    networkAvatarLocalPosition = (Vector3)stream.ReceiveNext();
                }
                else
                {
                    //Debug.Log("AvatarSync: Not Receiving Position");
                }

                //networkAvatarLocalPosition = (Vector3)stream.ReceiveNext();
                networkAvatarLocalRotation = (Quaternion)stream.ReceiveNext();
                networkAvatarHeadLocalRotation = (Quaternion)stream.ReceiveNext();

                //flash = (bool)stream.ReceiveNext();
            }
        }

        void Update()
        {
            // PV not ours - Set the locations of the objects to the ones received from the network
            if (!PV.IsMine)
            {
                if (trackPosition)
                {
                    // 
                    transform.localPosition = Vector3.Lerp(transform.localPosition, networkAvatarLocalPosition, Time.deltaTime * SmoothingDelay);
                }

                // Smoothly update the Avatars position and rotation
                //transform.localPosition = Vector3.Lerp(transform.localPosition, networkAvatarLocalPosition, Time.deltaTime * SmoothingDelay);
                transform.localRotation = Quaternion.Lerp(transform.localRotation, networkAvatarLocalRotation, Time.deltaTime * SmoothingDelay);
                AvatarHead.localRotation = Quaternion.Lerp(AvatarHead.localRotation, networkAvatarHeadLocalRotation, Time.deltaTime * SmoothingDelay);
 

                //FlashObject.SetActive(flash);
            }

            if (PV.IsMine)
            {

                if (mainCamera == null)
                {
                    mainCamera = Camera.main;
                }

                if (mainCamera != null)
                {
                    //Debug.Log("AvatarSync: Avatar postion - " + transform.position.ToString("F4"));

                    // Update the apprentices position to the main camera
                    transform.position = mainCamera.transform.position;
                    transform.eulerAngles = new Vector3(0, mainCamera.transform.rotation.eulerAngles.y, 0);
                    AvatarHead.rotation = mainCamera.transform.rotation;
                }

                //FlashObject.SetActive(myFlash);

            }
        }

        public void TrackPostion(bool track)
        {
            PV.RPC("RPC_PositionTracking", RpcTarget.AllBuffered, track);
        }

        [PunRPC]
        void RPC_PositionTracking(bool b)
        {
            if(PV == null)
            {
                PV = GetComponent<PhotonView>();
            }

            Debug.Log("AvatarSync: " + PV.name + " RPC_PositionTracking - " + b);
            trackPosition = b;
        }

        //IEnumerator Flash()
        //{
        //    myFlash = false;
        //    yield return new WaitForSeconds(15f);
        //    myFlash = true;
        //    StartCoroutine(Flash());
        //}

    }
}

