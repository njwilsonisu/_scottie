﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using VRTK;

namespace SCOTTIE
{
    public class AvatarManager : MonoBehaviour
    {

        public GameObject AvatarHead;
        public GameObject LeftHand;
        public GameObject RightHand;
        public GameObject AvatarBody;

        private Transform headsetTransform;
        private Transform leftHandTransform;
        private Transform rightHandTransform;
        private Transform playAreaTransform;

        // Start is called before the first frame update
        void Start()
        {
            AssignValues();
        }

        private void OnEnable()
        {
            Camera.onPreRender += OnCamPreRender;
        }

        public void AssignValues()
        {
            Debug.Log("Avatar Manager: Assign Vlaues");
            if (VRTK_DeviceFinder.DeviceTransform(VRTK_DeviceFinder.Devices.Headset) != null)
            {
                headsetTransform = VRTK_DeviceFinder.DeviceTransform(VRTK_DeviceFinder.Devices.Headset).gameObject.transform;
                leftHandTransform = VRTK_DeviceFinder.DeviceTransform(VRTK_DeviceFinder.Devices.LeftController).gameObject.transform;
                rightHandTransform = VRTK_DeviceFinder.DeviceTransform(VRTK_DeviceFinder.Devices.RightController).gameObject.transform;
                playAreaTransform = VRTK_DeviceFinder.PlayAreaTransform();
            }

        }

        protected virtual void OnCamPreRender(Camera cam)
        {
            if (headsetTransform == null || leftHandTransform == null || rightHandTransform == null || playAreaTransform == null)
            {
                AssignValues();
            }


            if (cam.gameObject.transform == VRTK_SDK_Bridge.GetHeadsetCamera())
            {
                Action();
            }

        }

        protected virtual void OnDisable()
        {
            Camera.onPreRender -= OnCamPreRender;
        }

        protected virtual void Action()
        {
            // The avatar follows the position of the HMD projected down to the play area floor
            FollowTransform(gameObject, headsetTransform, playAreaTransform, playAreaTransform);
            // The avatar's head exactly follows the position and rotation of the HMD
            FollowTransform(AvatarHead, headsetTransform, headsetTransform, headsetTransform);
            // The avatar's left hand exactly follows the position and rotation of the left VR controller
            FollowTransform(LeftHand, leftHandTransform, leftHandTransform, leftHandTransform);
            // The avatar's right hand exactly follows the position and rotation of the right VR controller
            FollowTransform(RightHand, rightHandTransform, rightHandTransform, rightHandTransform);
        }

        private static void FollowTransform(GameObject avatarComponent, Transform followXZ, Transform followY, Transform followRotation)
        {
            if (avatarComponent != null)
            {
                Vector3 pos = new Vector3(followXZ.position.x, followY.position.y, followXZ.position.z);
                avatarComponent.transform.position = pos;
                if (followRotation != null)
                {
                    avatarComponent.transform.rotation = followRotation.rotation;
                }
            }
        }
    }
}

