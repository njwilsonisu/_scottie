﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using JetBrains.Annotations;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.SceneManagement;

namespace SCOTTIE
{
    public class PhotonRoom : MonoBehaviourPunCallbacks, IInRoomCallbacks
    {

        public static PhotonRoom room;

        public bool isRoomLoaded;
        public bool isRoomFull;

        public Player[] photonPlayers;
        public int playersInRoom;
        public int myNumberInRoom;

        private enum userTypes
        {
            Undefined,
            Instructor,
            Apprentice
        }

        private userTypes userType;

        public GameObject photoLocationPrefab;

        private string prefabName = "NetworkPhotoLocation";
        
        public List<Transform> photoLocations = new List<Transform>();

        public void NewPhotoLocation(Transform location)
        {
            photoLocations.Add(location);
        }

        void Awake()
        {
            // this is our PhotoRoom instance
            PhotonRoom.room = this;

            // Define the usertype by the name of the scene that is playing
            if (SceneManager.GetActiveScene().name == "Instructor")
            {
                userType = userTypes.Instructor;
            }
            else if (SceneManager.GetActiveScene().name == "Apprentice")
            {
                userType = userTypes.Apprentice;
            }

            PhotonNetwork.AutomaticallySyncScene = true;
        }

        public override void OnEnable()
        {
            base.OnEnable();
            PhotonNetwork.AddCallbackTarget(this);
        }

        public override void OnDisable()
        {
            base.OnDisable();
            PhotonNetwork.RemoveCallbackTarget(this);
            photoLocations.Clear();
        }

        public override void OnJoinedRoom()
        {
            base.OnJoinedRoom();

            photonPlayers = PhotonNetwork.PlayerList;

            playersInRoom = photonPlayers.Length;

            myNumberInRoom = playersInRoom;

            PhotonNetwork.NickName = userType.ToString() + myNumberInRoom.ToString();

            // #Critical: We only load if we are the first player, else we rely on PhotonNetwork.AutomaticallySyncScene to sync our instance scene.
            if (PhotonNetwork.CurrentRoom.PlayerCount == 1)
            {
                Debug.Log("Loading SharedPlayground");

                // #Critical
                // Load the Room Level. 
                PhotonNetwork.LoadLevel("SharedPlayground");

            }

            StartGame();
        }

        public override void OnPlayerEnteredRoom(Player newPlayer)
        {
            base.OnPlayerEnteredRoom(newPlayer);
            photonPlayers = PhotonNetwork.PlayerList;
            playersInRoom++;

            Debug.Log("Player has entered: " + newPlayer.NickName);
            
        }

        void CreatePlayer()
        {
            // As the Instructor
            if (userType == userTypes.Instructor)
            {
                // Insantiate the Instructor Prefab where the camera is in the current local scene
                GameObject player = PhotonNetwork.Instantiate(Path.Combine("Prefabs", "PhotonUserInstructor"), Camera.main.transform.position, Camera.main.transform.rotation);
                
                // Make sure the player loads into the network scene
                DontDestroyOnLoad(player);

            }
            // As the Apprentice
            else if (userType == userTypes.Apprentice)
            {
                // Instantiate the Apprentice Prefab
                GameObject player = PhotonNetwork.Instantiate(Path.Combine("Prefabs", "PhotonUserApprentice"), Vector3.zero, Quaternion.identity);

                // Make sure the player loads into the network scene
                DontDestroyOnLoad(player);
            }
        }

        void StartGame()
        {
            // Create the player based on their identity
            CreatePlayer();

            // If this isnt the instructor, stop here
            if (userType != userTypes.Instructor)
            {
                return;
            }

            // Create the Network photo locations at the selected locations
            CreatePhotoLocations();
        }


        void CreatePhotoLocations()
        {
            //For loop for multiple locations
            int i = 0;

            Debug.Log("Creating Network Photo Location");

            //instantiate the prefab
            GameObject gObject = PhotonNetwork.Instantiate(Path.Combine("Prefabs", prefabName), photoLocations[0].position, photoLocations[0].rotation);

            gObject.name = "Network Photo Location " + i;

            DontDestroyOnLoad(gObject);

            Debug.Log("Network Photo Location Created");
        }
    }
}
