﻿using System.Collections;
using System.Collections.Generic;
using Microsoft.MixedReality.Toolkit;
using Microsoft.MixedReality.Toolkit.Input;
using UnityEngine;

namespace SCOTTIE
{

    public class LocateByCursor : MonoBehaviour
    {
        public GameObject CameraCursor;


        private void OnEnable()
        {
            //CoreServices.InputSystem?.FocusProvider?.SubscribeToPrimaryPointerChanged(OnPrimaryPointerChanged, true);
            CoreServices.InputSystem?.FocusProvider?.SubscribeToPrimaryPointerChanged(OnPrimaryPointerChanged, true);
        }

        private void OnPrimaryPointerChanged(IMixedRealityPointer oldPointer, IMixedRealityPointer newPointer)
        {

            if (CameraCursor != null)
            {
                if (newPointer != null)
                {
                    GameObject gameObjectReference = newPointer.BaseCursor?.GameObjectReference;
                    Transform parentTransform = (gameObjectReference != null) ? gameObjectReference.transform : null;

                    // If there's no cursor try using the controller pointer transform instead
                    if (parentTransform == null)
                    {
                        var controllerPointer = newPointer as BaseControllerPointer;
                        parentTransform = controllerPointer?.transform;
                    }

                    if (parentTransform != null)
                    {
                        CameraCursor.transform.SetParent(parentTransform, false);
                        CameraCursor.SetActive(true);
                        return;
                    }
                    else if (parentTransform == null)
                    {
                        Debug.Log(newPointer);
                    }
                }
            }
        }


        public void Disable()
        {
            CoreServices.InputSystem?.FocusProvider?.UnsubscribeFromPrimaryPointerChanged(OnPrimaryPointerChanged);

            //Debug.Log("Unsubscribed from Focus Provider");

            CameraCursor?.SetActive(false);

            //CameraCursor.transform.SetParent(null, false);

            gameObject.SetActive(false);
        }
    }
}

