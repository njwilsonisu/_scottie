﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using JetBrains.Annotations;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using VRTK;

namespace SCOTTIE
{

    // This script manages the players and objects for each of the users, once they connect to the network.
    public class NetworkManager : MonoBehaviourPunCallbacks
    {
        public static NetworkManager instance;

        public static event Action OnReadyToStartNetwork;
        // public static event Action OnNetworkStarted_CreatePlayer;
        
        private NetworkLauncher networkLauncher;

        public PhotonView localUser;

        private NetworkLauncher.userTypes localUserType;

        public Camera mainCamera;

        [HideInInspector]
        public bool followCamera = false;

        // Players in the room
        public Player[] photonPlayers;

        // number of players in the room
        public int playersInRoom;

        // The objects for the virtual reality users
        public GameObject[] virtualRealityToolkit;

        // the objects for the AR users
        public GameObject[] mixedRealityToolkit;

        // The photo locations
        public NetworkPhotoLocation[] networkPhotoLocations;


        void Awake()
        {
            // We need to make sure this is the only manager
            if (NetworkManager.instance == null)
            {
                NetworkManager.instance = this;
            }
            else
            {
                // If another players manager is already on the network, delete it and make this the instance
                if (NetworkManager.instance != this)
                {
                    networkPhotoLocations = NetworkManager.instance.networkPhotoLocations;
                    Destroy(NetworkManager.instance.gameObject);
                    NetworkManager.instance = this;
                }
            }

            DontDestroyOnLoad(this.gameObject);

            Debug.Log("Network Manager Created");

            playersInRoom = PhotonNetwork.CurrentRoom.PlayerCount;

            GetLocalUser();

            SetupToolkitObjects();
        }

        public void GetLocalUser()
        {
            // First, find the Network Launcher
            networkLauncher = NetworkLauncher.instance;

            if (networkLauncher != null)
            {
                Debug.Log("Network Manager: NetworkLauncher Located");

                localUser = networkLauncher.localUser;

                Debug.Log("Network Manager: the local user is " + localUser.name);

                // Get the local users type
                localUserType = networkLauncher.localUserType;

                Debug.Log("Network Manager: The local user's type is " + localUserType.ToString());
            }
        }

        public void SetupToolkitObjects()
        {
            if (localUser != null)
            {
                // If the local user is an instructor...
                if (localUserType == NetworkLauncher.userTypes.Instructor)
                {
                    // make sure the MRTK gameobjects are enabled
                    foreach (GameObject g in mixedRealityToolkit)
                    {
                        g.SetActive(true);
                        Debug.Log("Network Manager: Enabled MRTK gameobject: " + g.name);
                    }
                }
                // If the local user is a Apprentice...
                else if (localUserType == NetworkLauncher.userTypes.Apprentice)
                {

                    // Disable all the MRTK gameobjects
                    foreach (GameObject g in mixedRealityToolkit)
                    {
                        g.SetActive(false);
                        Debug.Log("Network Manager: Disabled MRTK gameobject: " + g.name);
                    }

                    // enable the VRTK gameobjects
                    foreach (GameObject g in virtualRealityToolkit)
                    {
                        g.SetActive(true);
                        Debug.Log("Network Manager: Enabled VRTK gameobject: " + g.name);
                    }
                }
            }
        }

        private void Start()
        {

            if (localUser != null)
            {
                // If the local user is an instructor...
                if (localUserType == NetworkLauncher.userTypes.Instructor)
                {
                    // Place the Instructor where he was in the previous scene, so the network objects are in the correct place
                    AdjustMRTKPosition();

                }
                // If the local user is a Apprentice...
                else if (localUserType == NetworkLauncher.userTypes.Apprentice)
                {
                    // Wait for the camera to be located
                    StartCoroutine(WaitForVRTKCamera());
                }
            }

            // get all the photon view objects and parent them
            ParentNetworkObjects();
        }

        public void AdjustMRTKPosition()
        {
            // Try to get the main camera in the scene
            mainCamera = Camera.main;

            // if the main camera was set...
            if (mainCamera != null)
            {
                Debug.Log("Network Manager: Main Camera Set - " + mainCamera.name);
            }

            // For some reason, MRTK wont let us reset the position of the camera to reflect where the user previously was
            // So we need to adjust the position of the network photo location instead

            GetNetworkPhotoLocations();

            // Find the "relative" transform of the photo location in relation to the instructor
            Debug.Log("Network Manager: Position of Local User - " + localUser.transform.position.ToString("F4"));
            Debug.Log("Network Manager: Position of " + networkPhotoLocations[0].name + " - " + networkPhotoLocations[0].transform.position.ToString("F4"));

            // Calculate what the local position of the network photo location would be if it were a child of the local user
            Vector3 relativePosition = localUser.transform.InverseTransformPoint(networkPhotoLocations[0].transform.position) + mainCamera.transform.position;
            Quaternion relativeRotation = Quaternion.Inverse(localUser.transform.rotation) * networkPhotoLocations[0].transform.rotation;

            // Move the network photo location to the corrected position
            networkPhotoLocations[0].transform.SetPositionAndRotation(relativePosition, relativeRotation);
            Debug.Log("Network Manager: UPDATED Position of " + networkPhotoLocations[0] + " - " + networkPhotoLocations[0].transform.position.ToString("F4"));

            // Then have Network Photo Location log it's new position
            networkPhotoLocations[0].SetNetworkLocation();
        }

        IEnumerator WaitForVRTKCamera()
        {
            Debug.Log("Network Manager: Waiting for Main Camera");

            // Wait for the apprentices main camera to be located
            while(mainCamera == null)
            {
                mainCamera = Camera.main;
                yield return null;
            }

            // The camera has been found
            Debug.Log("Network Manager: Main Camera Set - " + mainCamera.name);

            GetNetworkPhotoLocations();

            // If photo locations exist...
            if (networkPhotoLocations.Length > 0)
            {
                Debug.Log("Network Manager: Teleporting " + localUser.name + " to " + networkPhotoLocations[0].name);

                // Transport the Apprentice to the first photo location
                networkPhotoLocations[0].TeleportApprenticeHere();

            }
        }

        public void GetNetworkPhotoLocations()
        {
            Debug.Log("Network Manager: Getting Network Photo Locations");

            // get all the network photo locations
            networkPhotoLocations = GameObject.FindObjectsOfType<NetworkPhotoLocation>();

            Debug.Log("Network Manager: " + networkPhotoLocations.Length + " Network Photo Locations found");
        }

        public void ParentNetworkObjects()
        {
            // Set all the network tracked objects as children of this one

            // Get all of the PhotonViews in the scene
            PhotonView[] PhotonObjects = GameObject.FindObjectsOfType<PhotonView>();

            foreach (PhotonView p in PhotonObjects)
            {
                // if the object has no parent...
                if(p.transform.parent == null)
                {
                    // make it a child of this manager
                    Debug.Log("NetworkManager: Setting " + p.name + " as a child of " + gameObject.name);
                    p.transform.parent = transform;
                }
            }
        }

        public void FollowCamera()
        {
            Debug.Log("NetworkManager: FollowCamera");

            // We need the VR user to stay exactly where the 360 was taken
            // but VRTK doesnt have a good way to do this, so we fake it with this function

            // We need all of the objects to move with the users head movement
            // so we make the network manager follow the movement of the users head

            // Set the bool
            followCamera = true;

            // Calculate the distance between the network manager and this object
            Vector3 distance = mainCamera.transform.position - transform.position;

            // Then maintain that distance
            StartCoroutine(MaintainDistance(distance));
        }

        IEnumerator MaintainDistance(Vector3 distance)
        {

            // run this loop until the bool is set as false
            while (followCamera)
            {
                // Make the network manager follow the apprentices movement
                transform.position = mainCamera.transform.position - distance;

                yield return null;
            }

            // Reset the position
            transform.position = Vector3.zero;

        }

        //public void OnCancelButtonClicked()
        //{
        //    PhotonNetwork.LeaveRoom();
        //}

        public override void OnPlayerEnteredRoom(Player newPlayer)
        {
            base.OnPlayerEnteredRoom(newPlayer);

            photonPlayers = PhotonNetwork.PlayerList;

            playersInRoom = PhotonNetwork.CurrentRoom.PlayerCount;

            Debug.Log("Player has entered: " + newPlayer.UserId);

        }

    }
}

