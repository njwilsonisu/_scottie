﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using Photon.Pun;
using VRTK;

namespace SCOTTIE
{
    public class NetworkPhotoLocation : MonoBehaviour, IPunObservable
    {

        public TextMeshPro label;

        public GameObject photoSphere;

        public GameObject TeleportObject;

        private Quaternion networkRotation;
        private Vector3 networkPosition;

        private PhotonView PV;

        private PhotonView localUser;

        public bool ApprenticeIsHere = false;

        private NetworkManager networkManager;

        private NetworkLauncher networkLauncher;

        public NetworkManager NetworkManager
        {
            get
            {
                if (networkManager == null)
                {
                    // Try getting the network manager
                    networkManager = NetworkManager.instance;
                }

                return networkManager;
            }
        }

        void Start()
        {
            PV = GetComponent<PhotonView>();

            if (PV.IsMine)
            {
                Debug.Log("NetworkPhotoLocation: PhotonView is Mine");

                PV.RPC("RPC_SetNickName", RpcTarget.AllBuffered, gameObject.name);
            }

            networkLauncher = NetworkLauncher.instance;

            if (networkLauncher != null)
            {
                localUser = networkLauncher.localUser;

                Debug.Log("NetworkPhotoLocation: the local user - " + localUser.name);
            }
        }

        public void SetLabel(string lbl)
        {
            label.SetText(lbl);
        }

        public void SetNetworkLocation()
        {
            Debug.Log("NetworkPhotoLocation: Setting the Network Photo Location");

            object[] transformHolder = new object[]
            {
                    transform.position,
                    transform.rotation
            };

            PV.RPC("RPC_SetLocation", RpcTarget.AllBuffered, transformHolder);
        }



        // This function is only called on a local level of the apprentice
        public void TeleportApprenticeHere()
        {
            Debug.Log("NetworkPhotoLocation: TeleportHere()");

            Debug.Log("NetworkPhotoLocation: networkManager.followCamera = false");
            // Make sure the network manager doesnt track this movement
            NetworkManager.followCamera = false;

            Debug.Log("NetworkPhotoLocation: ApprenticeSync.TeleportApprentice(transform)");
            // Move the Apprentice
            localUser.GetComponent<ApprenticeSync>().TeleportToLocation(transform.position);

            Debug.Log("NetworkPhotoLocation: networkManager.FollowCamera()");
            // After the Apprentice has been moved, have the network manager follow the headset again
            NetworkManager.FollowCamera();

            // At a local level, we need to show the apprentice the correct objects
            TeleportObject.SetActive(false);
            label.gameObject.SetActive(false);
            photoSphere.SetActive(true);

            // Letting every instance of this photo location know that the apprentice is here
            PV.RPC("RPC_ApprenticeIsHere", RpcTarget.AllBuffered, true);
        }

        [PunRPC]
        void RPC_ApprenticeIsHere(bool b)
        {
            Debug.Log("NetworkPhotoLocation: RPC_ApprenticeIsHere");
            ApprenticeIsHere = b;
        }

        [PunRPC]
        void RPC_SetLocation(object[] posAndRot)
        {
            // Every time a new user connects to the network, this function will run

            if (PV == null)
            {
                PV = GetComponent<PhotonView>();
            }

            networkPosition = (Vector3)posAndRot[0];
            networkRotation = (Quaternion)posAndRot[1];

            Debug.Log("NetworkPhotoLocation: RPC_SetLocation - Position: " + networkPosition.ToString("F4") + " Rotation: " + networkRotation.eulerAngles.ToString("F4"));

            transform.position = networkPosition;
            transform.rotation = networkRotation;
        }

        [PunRPC]
        void RPC_SetNickName(string nName)
        {
            Debug.Log("NetworkPhotoLocation: RPC_SetNickName " + nName);
            gameObject.name = nName;

            if (NetworkManager != null)
            {
                transform.parent = NetworkManager.transform;

                Debug.Log("NetworkPhotoLocation: Set NetworkManager as parent");
            }
        }

        void IPunObservable.OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
        {
            //if (stream.IsWriting)
            //{
            //    // Stream the Photo Location
            //    stream.SendNext(transform.position);
            //    stream.SendNext(transform.rotation);

            //}
            //else
            //{
            //    networkPosition = (Vector3)stream.ReceiveNext();
            //    networkRotation = (Quaternion)stream.ReceiveNext();
            //}
        }
    }
}

