﻿using System.Collections;
using System.Collections.Generic;
using Microsoft.MixedReality.Toolkit.Input;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using UnityEngine.EventSystems;
using System.IO;
using VRTK;

namespace SCOTTIE
{
    // This Script manages how the Apprentice is represented in each users scene

    // This is only for the Apprentice as it doesnt continuously track position

    public class ApprenticeSync : MonoBehaviour, IPunObservable
    {
        private PhotonView PV;

        private PhotonUser photonUser;

        // Camera used for updating the avatars position
        [SerializeField]
        private Camera mainCamera;

        // higher value -> faster update
        public float SmoothingDelay = 10;

        public Transform AvatarHead;

        public GameObject AvatarBody;

        // is the user visible in the netowrk 3D space?
        private bool visible = true;

        // the users location on the network
        private Vector3 networkAvatarLocalPosition;

        // the rotation of the avatar around the y axis
        private Quaternion networkAvatarLocalRotation;

        // the rotation of the users head
        private Quaternion networkAvatarHeadLocalRotation;

        private GameObject LiveVideoArea;

        void Start()
        {
            PV = GetComponent<PhotonView>();
            photonUser = GetComponent<PhotonUser>();

            mainCamera = Camera.main;

            if (PV.IsMine)
            {
                Debug.Log("ApprenticeSync: PhotonView is Mine");
                
                DontDestroyOnLoad(gameObject);

                // I dont want to see my own head or body
                AvatarHead.gameObject.SetActive(false);
                AvatarBody.SetActive(false);

                // Create a viewing area
                LiveVideoArea = PhotonNetwork.Instantiate(Path.Combine("Prefabs", "StreamReceiver"), new Vector3(0,-1000,0), Quaternion.identity);
                // disable it to begin with
                LiveVideoArea.SetActive(false);
            }
        }

        void Update()
        {

            if (PV.IsMine)
            {
                // THIS IS OUR LOCAL SCENE --> Place the avater where the camera is

                // Make sure we have the main camera
                if (mainCamera == null)
                {
                    mainCamera = Camera.main;
                }

                // If we were able to get it...
                if (mainCamera != null)
                {
                    // Update the apprentices position to the main camera
                    transform.position = mainCamera.transform.position;

                    // Have the entire Avatar only rotate around the Y axis
                    transform.eulerAngles = new Vector3(0, mainCamera.transform.rotation.eulerAngles.y, 0);

                    // Have the Avatar Head follow the cameras position
                    AvatarHead.rotation = mainCamera.transform.rotation;
                }

            }
            // PV not ours - Set the rotations of the objects to the ones received from the network
            else
            {
                // Smoothly with Lerp
                transform.localRotation = Quaternion.Lerp(transform.localRotation, networkAvatarLocalRotation, Time.deltaTime * SmoothingDelay);
                AvatarHead.localRotation = Quaternion.Lerp(AvatarHead.localRotation, networkAvatarHeadLocalRotation, Time.deltaTime * SmoothingDelay);
            }


        }

        void IPunObservable.OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
        {

            if (stream.IsWriting)
            {
                // THIS IS OUR LOCAL SCENE --> we need to send the information
                stream.SendNext(transform.localRotation);
                stream.SendNext(AvatarHead.localRotation);
            }
            else
            {
                // THIS IS NOT OUR LOCAL SCENE --> we need to receive the information
                networkAvatarLocalRotation = (Quaternion)stream.ReceiveNext();
                networkAvatarHeadLocalRotation = (Quaternion)stream.ReceiveNext();
            }
        }

        // This function is used to set a new network position for the apprentice
        public void TeleportToLocation(Vector3 location)
        {
            Debug.Log("ApprenticeSync: TeleportApprentice to " + location);

            // For our local apprentice...
            if (PV.IsMine)
            {
                // THIS IS OUR LOCAL SCENE --> we need to transport the camera to the given location
                MoveCameraRigTo(location);

                // Update the location of the avatar
                transform.position = mainCamera.transform.position;
            }

            // Then send the new local position over the network
            PV.RPC("RPC_SetApprenticePosition", RpcTarget.AllBuffered, transform.localPosition);

        }

        public void MoveCameraRigTo(Vector3 location)
        {
            // THIS IS OUR LOCAL SCENE --> we need to transport the camera to the given location

            // get the transform of the current camera rig and the play area
            Transform headset = VRTK_DeviceFinder.HeadsetTransform();
            Transform playArea = VRTK_SDKManager.GetLoadedSDKSetup().transform;

            Debug.Log("ApprenticeSync: Headset Position - " + headset.position);
            Debug.Log("ApprenticeSync: Play Area Position - " + playArea.position);

            // calculate the difference between them
            Vector3 offest = headset.position - playArea.position;

            // place the play area so the headset is located at the position of the photolocation
            playArea.position = location - offest;

            Debug.Log("ApprenticeSync: Headset Position - " + headset.position);
            Debug.Log("ApprenticeSync: Play Area Position - " + playArea.position);
        }

        [PunRPC]
        void RPC_SetApprenticePosition(Vector3 pos)
        {
            Debug.Log("ApprenticeSync: RPC_SetApprenticePosition - " + pos);

            // Log the new position
            networkAvatarLocalPosition = pos;

            // make sure we have the PV
            if (PV == null)
            {
                PV = GetComponent<PhotonView>();
            }

            if(!PV.IsMine)
            {
                // THIS IS NOT OUR LOCAL SCENE --> we need to update where the avatar is
                transform.localPosition = networkAvatarLocalPosition;
            }
        }

        // This function teleports the apprentice to the first person view
        [PunRPC]
        void RPC_FirstPersonViewer()
        {
            Debug.Log("ApprenticeSync: RPC_FirstPersonViewer");

            // make sure we have the PV
            if (PV == null)
            {
                PV = GetComponent<PhotonView>();
            }

            if (PV.IsMine)
            {
                // THIS IS OUR LOCAL SCENE --> move the camera rig to the viewing area

                LiveVideoArea.SetActive(true);
                MoveCameraRigTo(LiveVideoArea.transform.position);
            }
            else
            {
                // THIS IS NOT OUR LOCAL SCENE --> disable the avatar
                gameObject.SetActive(false);
            }
        }

    }
}
