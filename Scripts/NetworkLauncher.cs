﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using UnityEngine.SceneManagement;
using Random = System.Random;

namespace SCOTTIE
{
    public class NetworkLauncher : MonoBehaviourPunCallbacks
    {
        // Instance of network launcher
        public static NetworkLauncher instance;

        // Option to try and connect to the network when the application starts
        public bool connectOnStart;

        // are we trying to connect?
        private bool isConnecting;

        //public bool isRoomFull;

        // Players in the room
        private Player[] photonPlayers;

        // number of players in the room
        private int playersInRoom;

        public PhotonView localUser;

        private GameObject myPlayer;

        public enum userTypes
        {
            Undefined,
            Instructor,
            Apprentice
        }

        public userTypes localUserType;

        private string prefabName = "NetworkPhotoLocation";

        // List of all the photo locations that the user has logged
        public List<Transform> photoLocations = new List<Transform>();


        // List of all the photo locations that the user has logged
        public List<GameObject> networkPhotoLocations = new List<GameObject>();

        // Function to log a new photolocation
        public void NewPhotoLocation(Transform location)
        {
            Debug.Log("NetworkLauncher: Photo Location Set: Position " + location.position + " - Rotation " + location.rotation);
            photoLocations.Add(location);
        }

        void Awake()
        {

            if (NetworkLauncher.instance == null)
            {
                NetworkLauncher.instance = this;
            }
            else
            {
                if (NetworkLauncher.instance != this)
                {
                    Destroy(NetworkLauncher.instance.gameObject);
                    NetworkLauncher.instance = this;
                }
            }

            DontDestroyOnLoad(this.gameObject);

            // Define the usertype by the name of the scene that is playing
            if (SceneManager.GetActiveScene().name == "Instructor")
            {
                localUserType = userTypes.Instructor;

            }
            else if (SceneManager.GetActiveScene().name == "Apprentice")
            {
                localUserType = userTypes.Apprentice;
            }
        }

        private void Start()
        {
            if (connectOnStart)
            {
                ConnectToNetwork();
            }

            SceneManager.sceneLoaded += OnLevelFinishedLoading;

        }

        public override void OnEnable()
        {
            base.OnEnable();
            PhotonNetwork.AddCallbackTarget(this);

        }

        public void ConnectToNetwork()
        {
            isConnecting = true;

            Debug.Log("NetworkLauncher: ConnectUsingSettings");

            // Then connect to the Network
            PhotonNetwork.ConnectUsingSettings();
        }

        // Once we have connected to the network...
        public override void OnConnectedToMaster()
        {
            Debug.Log("NetworkLauncher: OnConnectedToMaster");

            // If we arent trying to connect, dont do anything
            if (!isConnecting)
            {
                return;
            }

            // create a random user id
            int randomuserID = UnityEngine.Random.Range(0, 999999);

            PhotonNetwork.AuthValues = new AuthenticationValues();
            PhotonNetwork.AuthValues.UserId = randomuserID.ToString();

            Debug.Log("NetworkLauncher: Connected To Master - Joining Random Room");
            PhotonNetwork.JoinRandomRoom();
        }

        // If we joined a room that was already created by another user...
        public override void OnJoinedRoom()
        {
            base.OnJoinedRoom();

            Debug.Log("NetworkLauncher: OnJoinedRoom");

            Debug.Log("NetworkLauncher: RoomName: " + PhotonNetwork.CurrentRoom.Name);

            Debug.Log("NetworkLauncher: " + PhotonNetwork.CurrentRoom.PlayerCount + " Players in room");

            PhotonNetwork.NickName = localUserType.ToString() + PhotonNetwork.CurrentRoom.PlayerCount.ToString();
            //PhotonNetwork.NickName = PhotonNetwork.AuthValues.UserId;

            Debug.Log("NetworkLauncher: My Nickname is " + PhotonNetwork.NickName);

            photonPlayers = PhotonNetwork.PlayerList;

            playersInRoom = PhotonNetwork.CurrentRoom.PlayerCount;

            // Instantiate the Player and the necessary objects
            CreatePlayerObjects();

            //Debug.Log("NetworkLauncher: Position of Local User just before loading Shared Playground - " + Camera.main.transform.position.ToString("F4"));

            // #Critical: We only load if we are the first player, else we rely on PhotonNetwork.AutomaticallySyncScene to sync our instance scene.
            if (PhotonNetwork.CurrentRoom.PlayerCount == 1)
            {
                Debug.Log("NetworkLauncher: Loading SharedPlayground");

                // #Critical
                // Load the Room Level. 
                PhotonNetwork.LoadLevel("SharedPlayground");
            }
        }

        public void CreatePlayerObjects()
        {
            // As the Instructor
            if (localUserType == userTypes.Instructor)
            {
                Debug.Log("NetworkLauncher: Instantiating Instructor Prefab");

                // Insantiate the Instructor Prefab where the camera is in the current local scene
                GameObject player = PhotonNetwork.Instantiate(Path.Combine("Prefabs", "PhotonUserInstructor"), Camera.main.transform.position, Camera.main.transform.rotation);

                localUser = player.GetComponent<PhotonView>();

                Debug.Log("NetworkLauncher: Local User - " + localUser.name);

                myPlayer = player;

                myPlayer.name = PhotonNetwork.NickName;

                Debug.Log("NetworkLauncher: Renamed Player - " + player.name);

                // Make sure the player loads into the network scene
                DontDestroyOnLoad(player);

                // If the user is the Instructor and has logged photo locations...
                if (photoLocations.Count > 0)
                {
                    // Create the Network photo locations at the selected locations

                    //For loop for multiple locations
                    int i = 1;

                    Debug.Log("NetworkLauncher: Creating Network Photo Location");

                    //instantiate the NetworkPhotoLocation prefab
                    GameObject gObject = PhotonNetwork.Instantiate(Path.Combine("Prefabs", prefabName), photoLocations[0].position, photoLocations[0].rotation);

                    gObject.name = "Photo Location " + i;

                    gObject.GetComponent<NetworkPhotoLocation>().SetLabel(gObject.name);

                    networkPhotoLocations.Add(gObject);

                    DontDestroyOnLoad(gObject);

                    Debug.Log("NetworkLauncher: Network Photo Location Created - Position " + gObject.transform.position + " Rotation " + gObject.transform.rotation);
                }

            }
            // As the Apprentice
            else if (localUserType == userTypes.Apprentice)
            {
                // Instantiate the Apprentice Prefab
                GameObject player = PhotonNetwork.Instantiate(Path.Combine("Prefabs", "PhotonUserApprentice"), Vector3.zero, Quaternion.identity);

                localUser = player.GetComponent<PhotonView>();

                myPlayer = player;

                myPlayer.name = PhotonNetwork.NickName;

                Debug.Log("NetworkLauncher: Created Player: " + player.name);

                // Make sure the player loads into the network scene
                DontDestroyOnLoad(player);

                // If there is no other player in the room...
                if (playersInRoom == 1)
                {
                    //Instantiate a fake network photo location for the apprentice to be placed at until the instructor connects
                    Debug.Log("NetworkLauncher: Creating Fake Network Photo Location");

                    //instantiate the NetworkPhotoLocation prefab
                    GameObject gObject = PhotonNetwork.Instantiate(Path.Combine("Prefabs", prefabName), new Vector3(2, 0, 2), Quaternion.identity);

                    gObject.name = "Fake Photo Location";

                    gObject.GetComponent<NetworkPhotoLocation>().SetLabel(gObject.name);

                    DontDestroyOnLoad(gObject);

                    Debug.Log("NetworkLauncher: Fake Network Photo Location Created");
                }
            }

            Debug.Log("NetworkLauncher: Automatically Syncing Scene");
            PhotonNetwork.AutomaticallySyncScene = true;
        }

        void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode)
        {
            Debug.Log(scene.name + " Scene Loaded");

            Debug.Log("NetworkLauncher: Local User - " + localUser.name);

        }

        public override void OnPlayerEnteredRoom(Player newPlayer)
        {
            base.OnPlayerEnteredRoom(newPlayer);
            photonPlayers = PhotonNetwork.PlayerList;

            playersInRoom = PhotonNetwork.CurrentRoom.PlayerCount;

            //Debug.Log("NetworkLauncher: Player has entered: " + newPlayer.NickName);

        }

        public override void OnJoinRandomFailed(short returnCode, string message)
        {
            Debug.Log("NetworkLauncher: Random Room Join Failed, no available room");
            Debug.Log("NetworkLauncher: Trying to Create a New Room");

            CreateRoom();
        }

        void CreateRoom()
        {
            RoomOptions roomOptions = new RoomOptions() { IsVisible = true, IsOpen = true, MaxPlayers = 2 };
            PhotonNetwork.CreateRoom("SCOTTIE Room", roomOptions);
        }

        public override void OnCreateRoomFailed(short returnCode, string message)
        {
            Debug.Log("NetworkLauncher: Creating Room Failed");
            CreateRoom();
        }

        public override void OnCreatedRoom()
        {
            Debug.Log("NetworkLauncher: Room Created");
            base.OnCreatedRoom();
        }

        public void OnCancelButtonClicked()
        {
            PhotonNetwork.LeaveRoom();
        }

        public override void OnDisconnected(DisconnectCause cause)
        {
            //LogFeedback("<Color=Red>OnDisconnected</Color> " + cause);
            Debug.Log("NetworkLauncher: Disconnected from SharedPlayground");

            isConnecting = false;

        }



        public override void OnDisable()
        {
            base.OnDisable();
            PhotonNetwork.RemoveCallbackTarget(this);
            photoLocations.Clear();
            SceneManager.sceneLoaded -= OnLevelFinishedLoading;
        }
    }
}

