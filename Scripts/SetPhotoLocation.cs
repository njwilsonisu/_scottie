﻿using Microsoft.MixedReality.Toolkit;
using Microsoft.MixedReality.Toolkit.Input;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SCOTTIE
{

    public class SetPhotoLocation : MonoBehaviour
    {
        // the height of the tripod from the "floor" to the camera lense in meters
        public float tripodHeight;

        // is the marked location the bottom of the tripod?
        public bool tripod;

        // location in the scene where the bottom of the tripod is placed
        private Transform baseLocation;

        // location of the lense after height adjustment
        private Transform lenseLocation;

        // The Cursor handling object
        public LocateByCursor LocateByCursor;

        //the object used for final adjustments for the photos location and rotation
        public GameObject photoAdjustment;

        // the UI that allows the user to connect to the network
        public GameObject ConnectUI;

        // create a new photo location
        //public void NewPhoto(float tripodHeight, bool tripod, int locationNumber)
        //{
        //    this.tripodHeight = tripodHeight;
        //    this.tripod = tripod;
        //    this.locationNumber = locationNumber;
        //}

        private void Start()
        {
            if (ConnectUI != null)
            {
                ConnectUI.gameObject.SetActive(false);
            }

            LocatePhoto();
        }

        public void LocatePhoto()
        {
            // Enable the custom cursor
            ActivateLocateByCursor();
        }

        public void ActivateLocateByCursor()
        {
            if (LocateByCursor != null)
            {
                LocateByCursor.gameObject.SetActive(true);
            }
        }

        // called using the PointerHandler on the LocateByCursors
        public void SetLocationInitial(MixedRealityPointerEventData eventData)
        {
            // if the cursor is hitting the spatial observer mesh
            if (eventData.Pointer.IsTargetPositionLockedOnFocusLock)
            {
                // Disable the custom cursor object
                LocateByCursor.Disable();

                // The chosen location
                var result = eventData.Pointer.Result;

                // Place this PhotoLocation gameobject at the chosen
                transform.SetPositionAndRotation(result.Details.Point, Quaternion.identity);

                // Enable the PhotoAdjustment Object
                photoAdjustment.SetActive(true);

                // Wait for the user to press the "Done Button"
            }

        }

        // When the user presses the Done button...
        public void SetLocationFinal(Transform finalLocation)
        {
            //Print
            Debug.Log("Photo Location set \n" + finalLocation.position + "\n" + finalLocation.rotation);

            // Save the location to the network launcher
            NetworkLauncher networkLauncher = NetworkLauncher.instance;
            networkLauncher.NewPhotoLocation(finalLocation);

            // Deactivate the PhotoAdjustment
            photoAdjustment.SetActive(false);

            gameObject.SetActive(false);

            if (ConnectUI != null)
            {
                ConnectUI.transform.position = finalLocation.position + new Vector3(0, 0.2f, 0);
                ConnectUI.gameObject.SetActive(true);
            }
        }
    }
}
