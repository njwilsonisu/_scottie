﻿using System.Collections;
using System.Collections.Generic;
using Microsoft.MixedReality.Toolkit.Input;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using UnityEngine.EventSystems;
using VRTK;

namespace SCOTTIE
{
    public class HandSync_Apprentice : MonoBehaviour, IPunObservable
    {

        private PhotonView PV;

        // used for updating the avatars hand position
        public Transform VRTKHand_Right;
        public Transform VRTKHand_Left;

        // Avatar Hand
        public Transform avatarHand_Right;
        public Transform avatarHand_Left;

        // higher value -> faster
        public float SmoothingDelay = 10;

        private Vector3 networkHandLocalPosition_Right;
        private Quaternion networkHandLocalRotation_Right;

        private Vector3 networkHandLocalPosition_Left;
        private Quaternion networkHandLocalRotation_Left;


        void Start()
        {
            PV = GetComponent<PhotonView>();

            //This is the left hand
            VRTKHand_Left = VRTK_DeviceFinder.GetControllerLeftHand(true)?.transform;

            //This is the right hand
            VRTKHand_Right = VRTK_DeviceFinder.GetControllerRightHand(true)?.transform;

            //avatarHand_Left = Instantiate(VRTKHand_Left, VRTKHand_Left.position, VRTKHand_Left.rotation);
            
            //avatarHand_Right = Instantiate(VRTKHand_Right, VRTKHand_Right.position,VRTKHand_Right.rotation);
            
            if (PV.IsMine)
            {
                Debug.Log("HandSync Apprentice: PhotonView is Mine");

                // Turn off the Avatar representation
                avatarHand_Left.gameObject.SetActive(false);
                avatarHand_Right.gameObject.SetActive(false);
            }

        }

        void IPunObservable.OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
        {
            if (stream.IsWriting)
            {
                // We are the one sending the info

                // We send the current position of the Avatar hand
                stream.SendNext(avatarHand_Right.localPosition);
                stream.SendNext(avatarHand_Right.localRotation);

                stream.SendNext(avatarHand_Left.localPosition);
                stream.SendNext(avatarHand_Left.localRotation);
            }
            else
            {
                // We receive the info from the network
                networkHandLocalPosition_Right = (Vector3)stream.ReceiveNext();
                networkHandLocalRotation_Right = (Quaternion)stream.ReceiveNext();

                networkHandLocalPosition_Left = (Vector3)stream.ReceiveNext();
                networkHandLocalRotation_Left = (Quaternion)stream.ReceiveNext();
            }
        }

        void Update()
        {
            // PV not ours - Set the locations of the objects to the ones received from the network
            if (!PV.IsMine)
            {
                // Smoothly update the apprentices hand position
                avatarHand_Right.localPosition = Vector3.Lerp(avatarHand_Right.localPosition, networkHandLocalPosition_Right, Time.deltaTime * SmoothingDelay);
                avatarHand_Right.localRotation = Quaternion.Lerp(avatarHand_Right.localRotation, networkHandLocalRotation_Right, Time.deltaTime * SmoothingDelay);

                avatarHand_Left.localPosition = Vector3.Lerp(avatarHand_Left.localPosition, networkHandLocalPosition_Left, Time.deltaTime * SmoothingDelay);
                avatarHand_Left.localRotation = Quaternion.Lerp(avatarHand_Left.localRotation, networkHandLocalRotation_Left, Time.deltaTime * SmoothingDelay);
                
            }

            if (PV.IsMine)
            {
                if (VRTKHand_Right == null)
                {
                    VRTKHand_Right = VRTK_DeviceFinder.GetControllerRightHand(true)?.transform;
                }

                if (VRTKHand_Left == null)
                {
                    VRTKHand_Left = VRTK_DeviceFinder.GetControllerLeftHand(true)?.transform;
                }

                if (VRTKHand_Right != null)
                {
                    //if(avatarHand_Right == null)
                    //{
                    //    avatarHand_Right = Instantiate(VRTKHand_Right, VRTKHand_Right.position, VRTKHand_Right.rotation);

                    //    // Turn off the Avatar representation
                    //    avatarHand_Right.gameObject.SetActive(false);
                    //}

                    // Update the apprentices left hand position
                    avatarHand_Right.position = VRTKHand_Right.position;
                    avatarHand_Right.rotation = VRTKHand_Right.rotation;
                }

                if (VRTKHand_Left != null)
                {
                    //if (avatarHand_Left == null)
                    //{
                    //    avatarHand_Left = Instantiate(VRTKHand_Left, VRTKHand_Left.position, VRTKHand_Left.rotation);

                    //    // Turn off the Avatar representation
                    //    avatarHand_Left.gameObject.SetActive(false);
                    //}

                    // Update the apprentices left hand position
                    avatarHand_Left.position = VRTKHand_Left.position;
                    avatarHand_Left.rotation = VRTKHand_Left.rotation;
                }
            }
        }
    }
}

